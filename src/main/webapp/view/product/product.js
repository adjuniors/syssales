




app.controller('productsListController', function ($scope, $rootScope, productService, productCartService, $location) {

    $scope.sortInfo = {fields: ['id'], directions: ['asc']};
    $scope.products = {currentPage: 1};


    $scope.gridOptions = {
        data: 'products.list',
        useExternalSorting: true,
        sortInfo: $scope.sortInfo,

        columnDefs: [
            { field: 'idProduct', displayName: 'Id' },
            { field: 'name', displayName: 'Name' },
            { field: 'description', displayName: 'Description' },
            { field: 'price', displayName: 'Price' },
            { field: '', width: 30, cellTemplate: '<span class="glyphicon glyphicon-remove remove" ng-click="deleteRow(row)"></span>' }
        ],

        multiSelect: false,
        selectedItems: [],

        afterSelectionChange: function (rowItem) {
            if (rowItem.selected) {
                $rootScope.$broadcast('onSelectedTab', {tab : 0});
                $rootScope.$broadcast('productSelected', $scope.gridOptions.selectedItems[0].idProduct);

            }
        }
    };


    $scope.refreshGrid = function () {
        var listProductsArgs = {
            page: $scope.products.currentPage,
            sortFields: $scope.sortInfo.fields[0],
            sortDirections: $scope.sortInfo.directions[0]
        };

        productService.get(listProductsArgs, function (data) {
            $scope.products = data;
        })
    };


    $scope.deleteRow = function (row) {
        $rootScope.$broadcast('deleteProduct', row.entity.idProduct);
    };


    $scope.$watch('sortInfo', function () {
        $scope.products = {currentPage: 1};
        $scope.refreshGrid();
    }, true);


    $scope.$on('ngGridEventSorted', function (event, sortInfo) {
        $scope.sortInfo = sortInfo;
    });


    $scope.$on('refreshGrid', function () {
        $scope.refreshGrid();
    });


    $scope.$on('clear', function () {
        $scope.gridOptions.selectAll(false);
    });

    $scope.addProductCart = function(product) {
        let total = (product.quantity * product.price);
        var cart = {
            'idProduct': product.idProduct,
            'quantity': product.quantity,
            'name' : product.name,
            'total' : total,
            'finalized' : false
        };
        productCartService.save(cart).$promise.then(
            function () {
                $rootScope.$broadcast('addCartMsg');
                $location.path( "/cart" );
            },
            function () {
                $rootScope.$broadcast('error');
        });
    };
});


app.controller('productsFormController', function ($scope, $rootScope, productService) {


    $scope.clearForm = function () {

        $scope.product = null;

        document.getElementById('imageUrl').value = null;

        $scope.productForm.$setPristine();

        $rootScope.$broadcast('clear');
    };


    $scope.updateProduct = function () {
        productService.save($scope.product).$promise.then(
            function () {

                $rootScope.$broadcast('refreshGrid');

                $rootScope.$broadcast('saved');
                $scope.clearForm();
            },
            function () {

                $rootScope.$broadcast('error');
            });
    };


    $scope.$on('productSelected', function (event, id) {
        $scope.product = productService.get({id: id});
    });


    $scope.$on('deleteProduct', function (event, id) {
        productService.delete({id: id}).$promise.then(
            function () {

                $rootScope.$broadcast('refreshGrid');

                $rootScope.$broadcast('deleted');
                $scope.clearForm();
            },
            function () {

                $rootScope.$broadcast('error');
            });
    });
});








