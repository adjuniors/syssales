




app.controller('productsCartListController', function ($scope, $rootScope, productCartService, productCartConfirm, $location) {

    $scope.sortInfo = {fields: ['idCart'], directions: ['asc']};
    $scope.productsCart = {currentPage: 1};


    $scope.gridOptions = {
        data: 'productsCart.list',
        useExternalSorting: true,
        sortInfo: $scope.sortInfo,

        columnDefs: [
            { field: 'idCart', displayName: 'Id' },
            { field: 'name', displayName: 'Name' },
            { field: 'quantity', displayName: 'Quantity' },
            { field: 'total', displayName: 'Total' },
            { field: '', width: 30, cellTemplate: '<span class="glyphicon glyphicon-remove remove" ng-click="deleteRow(row)"></span>' }
        ],

        multiSelect: false,
        selectedItems: [],

        afterSelectionChange: function (rowItem) {
            if (rowItem.selected) {
                $rootScope.$broadcast('onSelectedTabCart', {tab : 0});
            }
        }
    };


    $scope.refreshGrid = function () {
        var listProductCartsArgs = {
            page: $scope.productsCart.currentPage,
            sortFields: $scope.sortInfo.fields[0],
            sortDirections: $scope.sortInfo.directions[0]
        };

        productCartService.get(listProductCartsArgs, function (data) {
            $scope.productsCart = data;
        })
    };


    $scope.deleteRow = function (row) {
        productCartService.delete({id: row.entity.idCart}).$promise.then(
            function () {

                $rootScope.$broadcast('refreshGridCart');

                $rootScope.$broadcast('deleted');
                $scope.clearForm();
            },
            function () {

                $rootScope.$broadcast('error');
            }
        );

    };


    $scope.$watch('sortInfoCart', function () {
        $scope.productsCart = {currentPage: 1};
        $scope.refreshGrid();
    }, true);


    $scope.$on('ngGridEventSortedCart', function (event, sortInfo) {
        $scope.sortInfoCart = sortInfo;
    });


    $scope.$on('refreshGridCart', function () {
        $scope.refreshGrid();
    });


    $scope.$on('clearCart', function () {
        $scope.gridOptions.selectAll(false);
    });

    $scope.confirmSale = function() {
        let confirm = {
            email : $scope.productsCart.email,
            carts : $scope.productsCart.list
        }
        productCartConfirm.save(confirm).$promise.then(
            function () {

                $rootScope.$broadcast('refreshGridCart');

                $rootScope.$broadcast('msgThanks');
                $location.path( "/" );
            },
            function () {
                $rootScope.$broadcast('error');
        });
    };

});











