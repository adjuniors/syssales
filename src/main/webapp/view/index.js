    var app = angular.module('Syssales', ['ngResource', 'ngGrid', 'ui.bootstrap', 'ngRoute', 'ngAnimate']);

    app.config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl : 'view/home.html',
                controller  : 'mainController'
            })
            .when('/product', {
                templateUrl : 'view/product/product.html'
            })
            .when('/cart', {
                templateUrl : 'view/cart/cart.html'
            })
            .otherwise({redirectTo:'/'});

    });


    app.controller('mainController', function($scope) {

    });

    app.controller('TabsProducts', function ($scope, $window, $rootScope) {

        $scope.active = 0;
        $scope.model = {
            name: 'Tabs'
        };

        $scope.$on("onSelectedTab", function(event, data){
            $scope.active = data.tab;
        });

    });

    app.controller('alertMessagesController', function ($scope) {

        $scope.$on('saved', function () {
            $scope.alerts = [
                { type: 'success', msg: 'Registo Salvo com sucesso!' }
            ];
        });


        $scope.$on('deleted', function () {
            $scope.alerts = [
                { type: 'success', msg: 'Registro deletado com sucesso !' }
            ];
        });


        $scope.$on('error', function () {
            $scope.alerts = [
                { type: 'danger', msg: 'Tivemos um problema com o servidor!' }
            ];
        });

        $scope.$on('addCartMsg', function () {
            $scope.alerts = [
                { type: 'success', msg: 'Produto adicionado ao carrinho!' }
            ];
        });



        $scope.$on('msgThanks', function () {
            $scope.alerts = [
                { type: 'success', msg: 'Obrigado por ter comprado! Um email foi enviado para você' }
            ];
        });

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
    });

    app.factory('productCartService', function ($resource) {
        return $resource('carts/:id');
    });

    app.factory('productCartConfirm', function ($resource) {
        return $resource('carts/finalized');
    });

    app.factory('productService', function ($resource) {
        return $resource('products/:id');
    });