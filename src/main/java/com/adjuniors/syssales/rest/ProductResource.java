package com.adjuniors.syssales.rest;

import com.adjuniors.syssales.data.Product;
import com.adjuniors.syssales.pagination.PaginatedListWrapperProduct;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * REST Service to expose the data to display in the UI grid.
 *
 *
 * @author Ademar Junior <ademarsjrs@gmail.com/>
 */
@Stateless
//@ApplicationPath("/product")
@Path("/products")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductResource extends Application {
    @PersistenceContext
    private EntityManager entityManager;

    private Integer countProducts() {
        Query query = entityManager.createQuery("SELECT COUNT(p.idProduct) FROM Product p");
        return ((Long) query.getSingleResult()).intValue();
    }

    @SuppressWarnings("unchecked")
    private List<Product> findProducts(int startPosition, int maxResults, String sortFields, String sortDirections) {
        Query query =
                entityManager.createQuery("SELECT p FROM Product p ORDER BY p." + sortFields + " " + sortDirections);
        query.setFirstResult(startPosition);
        query.setMaxResults(maxResults);
        return query.getResultList();
    }

    private PaginatedListWrapperProduct findProducts(PaginatedListWrapperProduct wrapper) {
        wrapper.setTotalResults(countProducts());
        int start = (wrapper.getCurrentPage() - 1) * wrapper.getPageSize();
        wrapper.setList(findProducts(start,
                                    wrapper.getPageSize(),
                                    wrapper.getSortFields(),
                                    wrapper.getSortDirections()));
        return wrapper;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public PaginatedListWrapperProduct listProducts(@DefaultValue("1")
                                            @QueryParam("page")
                                            Integer page,
                                                    @DefaultValue("id")
                                            @QueryParam("sortFields")
                                            String sortFields,
                                                    @DefaultValue("asc")
                                            @QueryParam("sortDirections")
                                            String sortDirections) {
        PaginatedListWrapperProduct paginatedListWrapperProduct = new PaginatedListWrapperProduct();
        paginatedListWrapperProduct.setCurrentPage(page);
        paginatedListWrapperProduct.setSortFields(sortFields);
        paginatedListWrapperProduct.setSortDirections(sortDirections);
        paginatedListWrapperProduct.setPageSize(10);
        return findProducts(paginatedListWrapperProduct);
    }

    @GET
    @Path("{id}")
    public Product getProduct(@PathParam("id") Long idProduct) {
        return entityManager.find(Product.class, idProduct);
    }

    @POST
    public Product saveProduct(Product product) {
        if (product.getIdProduct() == null) {
            Product productToSave = new Product();
            productToSave.setName(product.getName());
            productToSave.setDescription(product.getDescription());
            productToSave.setPrice(product.getPrice());
            productToSave.setImageUrl(product.getImageUrl());
            entityManager.persist(product);
        } else {
            Product productToUpdate = getProduct(product.getIdProduct());
            productToUpdate.setName(product.getName());
            productToUpdate.setDescription(product.getDescription());
            productToUpdate.setPrice(product.getPrice());
            productToUpdate.setImageUrl(product.getImageUrl());
            product = entityManager.merge(productToUpdate);
        }

        return product;
    }

    @DELETE
    @Path("{id}")
    public void deleteProduct(@PathParam("id") Long id) {
        entityManager.remove(getProduct(id));
    }
}
