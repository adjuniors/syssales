package com.adjuniors.syssales.rest;

import com.adjuniors.syssales.EmailService;
import com.adjuniors.syssales.data.Cart;
import com.adjuniors.syssales.data.ConfirmationSales;
import com.adjuniors.syssales.pagination.PaginatedListWrapperCart;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * REST Service to expose the data to display in the UI grid.
 *
 *
 * @author Ademar Junior <ademarsjrs@gmail.com/>
 */
@Stateless
//@ApplicationPath("/cart")
@Path("/carts")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CartResource extends Application {
    @PersistenceContext
    private EntityManager entityManager;

    private Integer countProductCart() {
        Query query = entityManager.createQuery("SELECT COUNT(c.idCart) FROM Cart c");
        return ((Long) query.getSingleResult()).intValue();
    }

    @SuppressWarnings("unchecked")
    private List<Cart> findProductCarts(int startPosition, int maxResults, String sortFields, String sortDirections) {
        Query query =
                entityManager.createQuery("SELECT c FROM Cart c WHERE finalized = false ORDER BY c." + sortFields + " " + sortDirections);
        query.setFirstResult(startPosition);
        query.setMaxResults(maxResults);
        return query.getResultList();
    }

    private PaginatedListWrapperCart findProductCarts(PaginatedListWrapperCart wrapper) {
        wrapper.setTotalResults(countProductCart());
        int start = (wrapper.getCurrentPage() - 1) * wrapper.getPageSize();
        wrapper.setList(findProductCarts(start,
                                    wrapper.getPageSize(),
                                    wrapper.getSortFields(),
                                    wrapper.getSortDirections()));
        return wrapper;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public PaginatedListWrapperCart listProductCarts(@DefaultValue("1")
                                            @QueryParam("page")
                                            Integer page,
                                                        @DefaultValue("id")
                                            @QueryParam("sortFields")
                                            String sortFields,
                                                        @DefaultValue("asc")
                                            @QueryParam("sortDirections")
                                            String sortDirections) {
        PaginatedListWrapperCart paginatedListWrapperCart = new PaginatedListWrapperCart();
        paginatedListWrapperCart.setCurrentPage(page);
        paginatedListWrapperCart.setSortFields(sortFields);
        paginatedListWrapperCart.setSortDirections(sortDirections);
        paginatedListWrapperCart.setPageSize(10);
        return findProductCarts(paginatedListWrapperCart);
    }

    @GET
    @Path("{id}")
    public Cart getProductCart(@PathParam("id") Long idCart) {
        return entityManager.find(Cart.class, idCart);
    }

    @POST
    public Cart saveProductCart(Cart cart) {
        if (cart.getIdCart() == null) {
            Cart cartToSave = new Cart();
            cartToSave.setIdProduct(cart.getIdProduct());
            cartToSave.setFinalized(cart.getFinalized());
            cartToSave.setQuantity(cart.getQuantity());
            cartToSave.setTotal(cart.getTotal());
            entityManager.persist(cart);
        } else {
            Cart cartToUpdate = getProductCart(cart.getIdCart());
            cartToUpdate.setIdProduct(cart.getIdProduct());
            cartToUpdate.setFinalized(cart.getFinalized());
            cartToUpdate.setQuantity(cart.getQuantity());
            cartToUpdate.setTotal(cart.getTotal());
            cart = entityManager.merge(cartToUpdate);
        }

        return cart;
    }

    @POST
    @Path("/finalized")
    public void finalizedSale(ConfirmationSales confirmationSales) throws EmailException {

        for (Cart cart : confirmationSales.getCarts()) {
            cart.setFinalized(true);
            entityManager.merge(cart);
        }
        new EmailService().send(confirmationSales.getEmail());

    }

    @DELETE
    @Path("{id}")
    public void deleteProductCart(@PathParam("id") Long id) {
        entityManager.remove(getProductCart(id));
    }

    private void sendEmail(String dest) throws EmailException {
        SimpleEmail email = new SimpleEmail();
        email.setHostName("aspmx.l.google.com");
        email.addTo(dest);
        email.setFrom("ademarsjrs@gmail.com");
        email.setSubject("Sua Primeira Compra");
        email.setMsg("Sua primeira compra foi realizada, seja bem vindo e muito obrigado");
        //email.setAuthentication("ademarsjrs@gmail.com", "kakau23%12");
        email.setSmtpPort(25);
        //email.setSSL(true);
        //email.setTLS(true);
        email.send();
    }
}
