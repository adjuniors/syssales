package com.adjuniors.syssales.data;

import javax.persistence.*;

/**
 *
 * @author Ademar Junior <ademarsjrs@gmail.com/>
 */

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idUser")
    @SequenceGenerator(name = "idUser", sequenceName = "idUser")
    private Long idUser;

    private String Name;

    private String email;

    private String senha;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }
}
