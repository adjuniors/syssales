package com.adjuniors.syssales.data;

import javax.persistence.*;

/**
  *
 * @author Ademar Junior <ademarsjrs@gmail.com/>
 */
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idProduct")
    @SequenceGenerator(name = "idProduct", sequenceName = "idProduct")
    private Long idProduct;

    private String name;

    private String description;

    private Double price;

    private String imageUrl;

    public Long getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Long idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String link) {
        this.imageUrl = link;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Product product = (Product) o;

        return idProduct.equals(product.idProduct);
    }

    @Override
    public int hashCode() {
        return idProduct.hashCode();
    }


}
