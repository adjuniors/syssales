package com.adjuniors.syssales.data;

import javax.persistence.*;

/**
 *
 * @author Ademar Junior <ademarsjrs@gmail.com/>
 */
@Entity
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idCart")
    @SequenceGenerator(name = "idCart", sequenceName = "idCart")
    private Long idCart;

    private Long idProduct;

    private String name;

    private Integer quantity;

    private Double total;

    private Boolean finalized;

    public Long getIdCart() {
        return idCart;
    }

    public void setIdCart(Long idCart) {
        this.idCart = idCart;
    }

    public Long getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Long idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Boolean getFinalized() {
        return finalized;
    }

    public void setFinalized(Boolean finalized) {
        this.finalized = finalized;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Cart cart = (Cart) o;

        return idCart.equals(cart.idCart);
    }

    @Override
    public int hashCode() {
        return idCart.hashCode();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
