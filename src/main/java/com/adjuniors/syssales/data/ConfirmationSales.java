package com.adjuniors.syssales.data;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Ademar Junior <ademarsjrs@gmail.com/>
 */
public class ConfirmationSales implements Serializable {

    private String email;

    private List<Cart> carts;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Cart> getCarts() {
        return carts;
    }

    public void setCarts(List<Cart> carts) {
        this.carts = carts;
    }
}
