package com.adjuniors.syssales.pagination;

import com.adjuniors.syssales.data.Product;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * Wraps all the information needed to paginate a table.
 *
 * @author Ademar Junior <ademarsjrs@gmail.com/>
 */
@XmlRootElement
public class PaginatedListWrapperProduct extends PaginatedListWrapper {

    @XmlElement
    private List<Product> list;

    public List getList() {
        return list;
    }

    public void setList(List<Product> list) {
        this.list = list;
    }
}
