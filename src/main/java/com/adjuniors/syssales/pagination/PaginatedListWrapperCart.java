package com.adjuniors.syssales.pagination;

import com.adjuniors.syssales.data.Cart;
import com.adjuniors.syssales.data.Product;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * Wraps all the information needed to paginate a table.
 *
 * @author Ademar Junior <ademarsjrs@gmail.com/>
 */
@XmlRootElement
public class PaginatedListWrapperCart extends PaginatedListWrapper {

    @XmlElement
    private List<Cart> list;

    public List getList() {
        return list;
    }

    public void setList(List<Cart> list) {
        this.list = list;
    }
}