# Sys Sales - é um projeto exemplo de comunicação do Angularjs  com o Java via Rest API  #


## Para Fazer ele funcinar ? ##

### Comandos ###

instale o Node  http://nodejs.org/download/
instale o Maven

no diretório

* npm install bower
* bower install
* mvn install

### Executar o projeto ""

* `mvn tomee:run`
* o projeto vai esta por padrão no endereço http://localhost:8080/syssales/ (http://localhost:8080/syssales/)


### SQL  ###
as sql de insert, update, delete estão em:
src\main\resources\sql\insert.sql
src\main\resources\sql\update.sql
src\main\resources\sql\delete.sql

e serão excutadas pelo java persistent ao iniciar a aplicação.